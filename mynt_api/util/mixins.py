from django.http import JsonResponse
from django.conf import settings
from django.http import HttpResponse

from mynt_api.core.models import ExchangeRate


class CurrencyHandler(object):
    url_params = list()

    def dispatch(self, request, *args, **kwargs):
        for url_param in self.url_params:
            if url_param in kwargs:
                if not ExchangeRate.objects.filter(base__iexact=kwargs[url_param]).exists():
                    return JsonResponse(dict(
                        error="You must provide a valid currency. {} is not valid. "
                              "Please review the available currencies.".format(kwargs[url_param])
                    ), status=400)
                else:
                    setattr(self, url_param, kwargs[url_param].upper())
            else:
                setattr(self, url_param, settings.BASE_CURRENCY)

        return super(CurrencyHandler, self).dispatch(request, *args, **kwargs)


class JSONPCapable(object):
    def dispatch(self, request, *args, **kwargs):
        response = super(JSONPCapable, self).dispatch(request, *args, **kwargs)

        if response.status_code == 200 and "callback" in request.GET:
            content = request.GET['callback'] + "({})".format(response.content)
            return HttpResponse(content, content_type="text/javascript; charset=utf-8")

        return response
