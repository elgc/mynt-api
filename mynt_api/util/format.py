def output_rate(rate):
    return '%.4f' % rate


def output_date(date):
    return date.strftime('%m-%d-%Y')
