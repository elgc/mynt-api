from __future__ import unicode_literals

from django.db import models


class ExchangeRate(models.Model):
    """
    This model defines the exchange rate for one currency against another
    """
    base = models.CharField('base currency', max_length=8)
    versus = models.CharField('versus currency', max_length=8)
    rate = models.DecimalField('rate', max_digits=16, decimal_places=8, default=0.0)
    date = models.DateField('date')

    def __str__(self):
        return '1 {} = {} {} | {}'.format(self.base, self.rate, self.versus, self.date)
