import datetime
import collections

from django.views.generic import View
from django.http import JsonResponse

from mynt_api.util.mixins import CurrencyHandler, JSONPCapable
from mynt_api.util.format import output_rate, output_date

from .models import ExchangeRate


# Create your views here.

class LatestView(CurrencyHandler, JSONPCapable, View):
    url_params = ['currency']

    def get(self, request, *args, **kwargs):
        latest_date = ExchangeRate.objects.filter(base=self.currency).latest('date').date
        qs = ExchangeRate.objects.filter(base=self.currency, date=latest_date).order_by('versus')

        data = dict(
            base=self.currency,
            date=output_date(latest_date),
            rates=dict([(exchange_rate.versus, output_rate(exchange_rate.rate)) for exchange_rate in qs]),
        )

        return JsonResponse(data, status=200)


class LatestFaceToFaceView(CurrencyHandler, JSONPCapable, View):
    url_params = ['base', 'versus']

    def get(self, request, *args, **kwargs):
        if self.base == self.versus:
            return JsonResponse(dict(
                base=self.base,
                versus=self.versus,
                date=output_date(datetime.date.today()),
                rate=1
            ), status=200)

        er = ExchangeRate.objects.filter(base=self.base, versus=self.versus).latest('date')

        data = dict(
            base=self.base,
            versus=self.versus,
            date=output_date(er.date),
            rate=output_rate(er.rate)
        )

        return JsonResponse(data, status=200)


class HistoricalFaceToFaceView(CurrencyHandler, JSONPCapable, View):
    url_params = ['base', 'versus']

    def get(self, request, *args, **kwargs):
        data = dict(
            base=self.base,
            versus=self.versus
        )

        query = dict()
        single_date = False

        date = self.request.GET.get('date')
        start_date = self.request.GET.get('start')
        end_date = self.request.GET.get('end')

        if not date and not start_date and not end_date:
            return JsonResponse(dict(
                error="Ensure you provide a date or date range."
            ), status=400)

        if date:
            try:
                date = datetime.datetime.strptime(self.request.GET.get('date'), "%m-%d-%Y")
                query.update(dict(date=date.strftime('%Y-%m-%d')))
                single_date = True
            except ValueError:
                return JsonResponse(dict(
                    error="The required format for dates is mm-dd-yyyy. Please review you request."
                ), status=400)

        else:
            if not start_date or not end_date:
                return JsonResponse(dict(
                    error="Please, provide a valid date range. (start and end params are both required)"
                ), status=400)

            try:
                start_date = datetime.datetime.strptime(start_date, "%m-%d-%Y")
                end_date = datetime.datetime.strptime(end_date, "%m-%d-%Y")
            except (TypeError, ValueError):
                return JsonResponse(dict(
                    error="The required format for dates is mm-dd-yyyy. "
                          "Please review you request."
                ), status=400)

            if start_date > end_date:
                return JsonResponse(dict(
                    error="Ensure you provide a logic date range",
                ), status=400)

            query.update(dict(date__range=(start_date.strftime('%Y-%m-%d'), end_date.strftime('%Y-%m-%d'))))

        if self.base == self.versus:
            data.update()
            if single_date:
                data.update(
                    date=output_date(date),
                    rate=1
                )
            else:
                data.update(
                    start=output_date(start_date),
                    end=output_date(end_date),
                    rates={'{}'.format(output_date(end_date)): 1}
                )

            return JsonResponse(data, status=200)

        query.update(base=self.base, versus=self.versus)
        qs = ExchangeRate.objects.filter(**query).order_by('date')

        if single_date:
            exchange_rate = qs.first()
            data.update(dict(
                date=output_date(exchange_rate.date) if exchange_rate else output_date(date)),
                rate=output_rate(exchange_rate.rate) if exchange_rate else "Sorry, no data in DB."
            )
        else:
            d = [(output_date(exchange_rate.date), output_rate(exchange_rate.rate)) for exchange_rate in qs]
            d.sort()

            ordered_data = collections.OrderedDict()
            for e in d:
                ordered_data.update({"{}".format(e[0]): e[1]})

            data.update(dict(
                start=output_date(start_date),
                end=output_date(end_date),
                rates=ordered_data,
            ))

        return JsonResponse(data, status=200)


class AvailableCurrenciesView(JSONPCapable, View):
    def get(self, request, *args, **kwargs):
        qs = ExchangeRate.objects.all().distinct('base').values_list('base', flat=True)

        return JsonResponse([currency for currency in qs], status=200, safe=False)
