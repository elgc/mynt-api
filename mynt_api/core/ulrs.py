from django.conf.urls import url

from .views import *

urlpatterns = [
    url(
        regex=r'^latest/$',
        view=LatestView.as_view(),
        name='latest'
    ),
    url(
        regex=r'^latest/(?P<currency>[a-zA-Z]+)/$',
        view=LatestView.as_view(),
        name='latest'
    ),
    url(
        regex=r'^latest/(?P<base>[a-zA-Z]+)-(?P<versus>[a-zA-Z]+)/$',
        view=LatestFaceToFaceView.as_view(),
        name='face-to-face'
    ),
    url(
        regex=r'^historical/(?P<base>[a-zA-Z]+)-(?P<versus>[a-zA-Z]+)/$',
        view=HistoricalFaceToFaceView.as_view(),
        name='historical'
    ),
    url(
        regex=r'^available-currencies/$',
        view=AvailableCurrenciesView.as_view(),
        name='historical'
    ),
]
