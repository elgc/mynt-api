import requests

from xml.etree import ElementTree
import djclick as click

from django.conf import settings

from mynt_api.core.models import ExchangeRate

DATA_TAG_POSITION = 2


@click.command()
@click.option('--days-before', type=int, default=1)
def update(days_before):
    response = requests.get(settings.EXCHANGE_RATE_DATA_URL)
    tree = ElementTree.fromstring(response.content)

    for i in xrange(days_before):
        _date = tree[DATA_TAG_POSITION][i].attrib['time']
        print _date
        data = dict()
        raw_data = tree[DATA_TAG_POSITION][i]
        print raw_data
        try:
            for exchange_rate_element in raw_data:
                data.update(
                    {'{}'.format(exchange_rate_element.attrib['currency']): exchange_rate_element.attrib['rate']})

            print "Data get: {}".format(data)

            # EUR to all
            for currency, rate in data.items():
                ExchangeRate.objects.get_or_create(
                    base=settings.BASE_CURRENCY,
                    versus=currency.upper(),
                    date=_date,
                    defaults=dict(
                        rate=rate
                    )
                )

                ExchangeRate.objects.get_or_create(
                    base=currency.upper(),
                    versus=settings.BASE_CURRENCY,
                    date=_date,
                    defaults=dict(
                        rate=1 / float(rate)
                    )
                )

            for currency, base_rate in data.items():
                base = currency
                for versus, rate in data.items():
                    if versus != base:
                        ExchangeRate.objects.get_or_create(
                            base=base.upper(),
                            versus=versus.upper(),
                            date=_date,
                            defaults=dict(
                                rate=float(rate) / float(base_rate)
                            )
                        )
        except Exception:
            continue


if __name__ == "__main__":
    update()
