from django.contrib import admin

from mynt_api.core.models import ExchangeRate


# Register your models here.

class ExchangeRateAdmin(admin.ModelAdmin):
    list_display = [
        'base',
        'versus',
        'rate',
        'date'
    ]

    list_filter = [
        'base',
        'versus',
        'date'
    ]

    search_fields = [
        'base',
        'versus'
    ]


admin.site.register(ExchangeRate, ExchangeRateAdmin)
