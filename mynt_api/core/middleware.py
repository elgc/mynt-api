from django.http import JsonResponse


class JSONResponsesMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        response = self.get_response(request)

        if response.status_code == 404:
            return JsonResponse(dict(
                error="Unrecognized request URL."
            ), status=404)

        if response.status_code > 500:
            return JsonResponse(dict(
                error="API Error."
            ), status=response.status_code)

        return response
